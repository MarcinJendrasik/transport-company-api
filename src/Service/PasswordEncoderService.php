<?php


namespace App\Service;


use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class PasswordEncoderService
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;

    }

    public function encodePassword(UserInterface $user) {
        $encodedPassword = $this->passwordEncoder->encodePassword($user, $user->getConfirmPassword());
        $user->setPassword($encodedPassword);
        return $user;
    }
//    public function confirmedPassword(UserInterface $user) {
//        $encodedPassword = $this->passwordEncoder->encodePassword($user, $user->getPassword());
//        $user->setPassword($encodedPassword);
//        return $user;
//    }

}