<?php
/**
 * Created by PhpStorm.
 * User: martino
 * Date: 28.10.20
 * Time: 10:46
 */

namespace App\Service;


use App\Repository\DistrictRepository;

class DistrictService
{
    private $districtRepository;

    /**
     * DistrictService constructor.
     * @param $districtRepository
     */
    public function __construct(DistrictRepository $districtRepository)
    {
        $this->districtRepository = $districtRepository;
    }

    public function getDistricts() {
        return $this->districtRepository->findAll();
    }
}