<?php


namespace App\Service;

use App\Repository\UserRepository;
use FOS\UserBundle\Model\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

class UserService
{
    private $tokenStorage;
    private $userRepository;

    public function __construct(TokenStorageInterface $tokenStorage,
                                UserRepository $userRepository) {
        $this->tokenStorage = $tokenStorage;
        $this->userRepository = $userRepository;
    }

    private function getUserFromToken():\App\Entity\User {
        return $this->tokenStorage->getToken()->getUser();
    }

    public function getUsers() {

        $currentUser = $this->getUserFromToken();
        if ($currentUser->getRole() == "ROLE_ADMIN") {
            $users = $this->userRepository->findAll();
        } else {
            $users = $this->userRepository->findBy(["id" => $currentUser->getId()]);
        }
        return $users;
    }

    public function getUserById($id) {

        $currentUser = $this->getUserFromToken();

        $user = null;
        if ($currentUser->getRole() == "ROLE_ADMIN") {
            $user = $this->userRepository->findOneBy(["id" => $id]);
        } else {
            if ($this->isCurrentUserHasId($currentUser, $id)) {
                $user = $this->userRepository->findOneBy(["id" => $id]);
            } else {
                throw new AccessDeniedException('Unable to access this page!');
            }
        }
        return $user;
    }

    /**
     * @param UserInterface $currentUser
     * @param $id
     * @return bool
     */
    public function isCurrentUserHasId(UserInterface $currentUser, $id): bool
    {
        return $currentUser->getId() == $id;
    }
}

