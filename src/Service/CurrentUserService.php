<?php
/**
 * Created by PhpStorm.
 * User: martino
 * Date: 30.03.20
 * Time: 21:38
 */

namespace App\Service;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CurrentUserService
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage) {
        $this->tokenStorage = $tokenStorage;
    }

    public function getCurrentUser() {
        return $this->tokenStorage->getToken()->getUser();
    }
}