<?php


namespace App\Service;

use App\Entity\UserOrder;
use App\Model\Status;
use App\Model\StatusValueObject;
use App\Repository\CourierRepository;
use App\Repository\UserOrderRepository;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

class CourierOrderService
{
    private $tokenStorage;
    private $userOrderRepository;
    private $courierRepository;

    public function __construct(TokenStorageInterface $tokenStorage,
                                CourierRepository $courierRepository,
                                UserOrderRepository $userOrderRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->courierRepository = $courierRepository;
        $this->userOrderRepository = $userOrderRepository;
    }

    /**
     * @return object|UserInterface
     */
    private function getUserFromToken(): UserInterface
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCourierOrderAll()
    {
        $currentUser = $this->getUserFromToken();

        if ($currentUser->getRole() == "ROLE_ADMIN") {
            return $this->userOrderRepository->findAll();
        } else if ($currentUser->getRole() == "ROLE_COURIER") {
            $courier = $this->courierRepository->findOneBy(["user" => $currentUser]);
            $this->checkIsThisCourier($courier);

            return $this->userOrderRepository->findBy(["courier" => $courier]);
        } else {
            throw new AccessDeniedException('Unable to access this page!');
        }
    }

    /**
     * @param $id
     * @throws Exception
     * @throws AccessDeniedException
     * @return object
     * */
    public function getCourierOrderById($id)
    {
        $currentUser = $this->getUserFromToken();

        if ($currentUser->getRole() == "ROLE_ADMIN") {
            return $this->userOrderRepository->findOneBy(["id" => $id]);
        } else if ($currentUser->getRole() == "ROLE_COURIER") {
            $courier = $this->courierRepository->findOneBy(["user" => $currentUser]);
            $this->checkIsThisCourier($courier);

            $courierOrder = $this->userOrderRepository->findOneBy(["courier" => $courier, "id" => $id]);
            $this->checkIsThisCourierOrder($id, $courierOrder, $courier);
            return $courierOrder;
        } else {
            throw new AccessDeniedException('Unable to access this page!');
        }
    }

    /**
     * @param $courier
     * @return bool
     * @throws Exception
     * @throws AccessDeniedException
     */
    public function isNotCourier($courier): bool
    {
        return $courier == null;
    }

    public function getStatusesById($id) {
        $currentUser = $this->getUserFromToken();

        if ($currentUser->getRole() == "ROLE_ADMIN") {
            return [
                new StatusValueObject(Status::CREATED),
                new StatusValueObject(Status::RECEIVED_FROM_SENDER),
                new StatusValueObject(Status::LOCALIZE_HEADQUARTERS),
                new StatusValueObject(Status::DURING_TO_RECIPIENT),
                new StatusValueObject(Status::PROVIDED)
            ];
        } else if ($currentUser->getRole() == "ROLE_COURIER") {
            $courier = $this->courierRepository->findOneBy(["user" => $currentUser]);
            $this->checkIsThisCourier($courier);

            $courierOrder = $this->userOrderRepository->findOneBy(["courier" => $courier, "id" => $id]);
            $this->checkIsThisCourierOrder($id, $courierOrder, $courier);
            return $this->getAvailableStatusesByStatus($courierOrder->getStatus());
        } else {
            throw new AccessDeniedException('Unable to access this page!');
        }
    }

    private function getAvailableStatusesByStatus($status) {
        switch ($status) {
            case Status::CREATED:
                return [new StatusValueObject(Status::RECEIVED_FROM_SENDER)];
            case Status::RECEIVED_FROM_SENDER:
                return [new StatusValueObject(Status::LOCALIZE_HEADQUARTERS)];
            case Status::LOCALIZE_HEADQUARTERS:
                return [new StatusValueObject(Status::DURING_TO_RECIPIENT)];
            case Status::DURING_TO_RECIPIENT:
                return [new StatusValueObject(Status::PROVIDED)];
            default:
                return [];
        }
    }

    /**
     * @param $id
     * @param $courierOrder
     * @param $courier
     * @return mixed
     * @throws Exception
     */
    public function checkIsThisCourierOrder($id, $courierOrder, $courier)
    {
        if ($courierOrder === null) {
            throw new \Exception("This courier order by id:$id not belongs to this courier by id:" . $courier->getId());
        }
    }

    /**
     * @param $courier
     * @throws Exception
     */
    public function checkIsThisCourier($courier): void
    {
        if ($this->isNotCourier($courier)) {
            throw new \Exception("Data inconsistency. Logged user is not a courier");
        }
    }
}