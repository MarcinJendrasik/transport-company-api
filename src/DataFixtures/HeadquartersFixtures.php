<?php


namespace App\DataFixtures;

use App\Entity\Headquarters;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class HeadquartersFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
      $headquarters = new Headquarters();
      $headquarters->setName('main headquarter');
      $headquarters->setCity('Warsaw');
      $headquarters->setStreet('Mazowiecka');
      $headquarters->setHouseNumber('49');
      $headquarters->setApartmentNumber('18');
      $headquarters->setVoivodeship('Mazovian');
      $headquarters->setEmail('headquarters@head.com');
      $headquarters->setPhoneNumber('777-888-999');

      $manager->persist($headquarters);
      $manager->flush();

    }
}