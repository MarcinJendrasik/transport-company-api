<?php


namespace App\DataFixtures;

use App\Entity\User;
use App\Service\PasswordEncoderService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AdminFixtures extends Fixture
{
    private $passwordEncoderService;

    public function __construct(PasswordEncoderService $passwordEncoderService)
    {
        $this->passwordEncoderService = $passwordEncoderService;
    }

    public function load(ObjectManager $manager)
    {
        $email = 'admin@admin.pl';
        $user = new User();    // tworzenie obiektu
        $user->setName('Admin_Kamil');
        $user->setSurname('Gronek');
        $user->setRole('ROLE_ADMIN');
        $user->setPassword('admin');
        $user->setConfirmPassword("admin");
        $this->passwordEncoderService->encodePassword($user);
        $user->setEmail($email);
        $user->setUsernameCanonical($email);
        $user->setEnabled(true);

        $manager->persist($user);
        $manager->flush();
    }
}

