<?php


namespace App\Form;


use App\Entity\UserOrder;
use App\Model\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserOrderRecipientCourierStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("status",  ChoiceType::class, [
                'choices'  => [
                    'DURING TO RECIPIENT' => Status::DURING_TO_RECIPIENT,
                    'PROVIDED' => Status::PROVIDED
                 ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserOrder::class
        ]);
    }
}