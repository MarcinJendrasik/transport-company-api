<?php
/**
 * Created by PhpStorm.
 * User: martino
 * Date: 03.11.20
 * Time: 21:54
 */

namespace App\Model;


class StatusValueObject
{
    private $name;
    private $value;

    /**
     * StatusValueObject constructor.
     * @param string $status
     */
    public function __construct($status)
    {
        switch ($status) {
            case Status::CREATED:
                $this->name = "Created";
                $this->value = Status::CREATED;
                break;
            case Status::RECEIVED_FROM_SENDER:
                $this->name = "Received from sender";
                $this->value = Status::RECEIVED_FROM_SENDER;
                break;
            case Status::LOCALIZE_HEADQUARTERS:
                $this->name = "Localize headquarters";
                $this->value = Status::LOCALIZE_HEADQUARTERS;
                break;
            case Status::DURING_TO_RECIPIENT:
                $this->name = "During to recipient";
                $this->value = Status::DURING_TO_RECIPIENT;
                break;
            case Status::PROVIDED:
                $this->name = "Provided";
                $this->value = Status::PROVIDED;
                break;
        }
    }
}