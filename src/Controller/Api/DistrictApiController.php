<?php
/**
 * Created by PhpStorm.
 * User: martino
 * Date: 28.10.20
 * Time: 10:41
 */

namespace App\Controller\Api;

use App\Service\DistrictService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/api/district")
 */
class DistrictApiController extends AbstractFOSRestController
{

    private $districtService;

    public function __construct(DistrictService $districtService)
    {
        $this->districtService = $districtService;
    }

    /**
     * @Rest\Get("/")
     * @return View
     */
    public function index(): View
    {
        $districts = $this->districtService->getDistricts();
        return View::create($districts, Response::HTTP_OK);
    }
}