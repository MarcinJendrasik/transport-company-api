<?php

namespace App\Controller\Api;

use App\Entity\Headquarters;
use App\Form\HeadquartersType;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api/headquarters")
 */
class HeadquartersApiController extends AbstractController
{
    /**
     * @Rest\Get("/")
     * @return View
     */
    public function index(): View
    {
        $headquartersList = $this->getDoctrine()
            ->getRepository(Headquarters::class)
            ->findAll();

        return View::create($headquartersList, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/new")
     * @param $request
     * @return View
     */
    public function new(Request $request): View
    {
        $headquarters = new Headquarters();

        $form = $this->createForm(HeadquartersType::class, $headquarters, [
            'csrf_protection' => false,
            'method' => 'POST'
        ]);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($headquarters);
            $entityManager->flush();

            return View::create($headquarters, Response::HTTP_CREATED);
        }

        return View::create($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Get("/{id}")
     * @param $headquarters
     * @return View
     */
    public function show(Headquarters $headquarters): View
    {
        return View::create($headquarters, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/{id}/edit")
     * @param $request
     * @param $headquarters
     * @return View
     */
    public function edit(Request $request, Headquarters $headquarters): View
    {
        $form = $this->createForm(HeadquartersType::class, $headquarters, [
            'csrf_protection' => false,
            'method' => 'PUT'
        ]);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return View::create($headquarters, Response::HTTP_OK);
        }

        return View::create($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Delete("/{id}")
     * @param $headquarters
     * @return View
     */
    public function delete(Headquarters $headquarters): View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($headquarters);
        $entityManager->flush();

        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}

