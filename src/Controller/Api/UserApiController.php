<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Form\UserType;
use App\Service\PasswordEncoderService;
use App\Service\UserService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

/**
 * @Route("/api/user")
 */
class UserApiController extends AbstractFOSRestController
{
    private $userService;
    private $passwordEncoderService;

    public function __construct(UserService $userService,
                                PasswordEncoderService $passwordEncoderService) {

        $this->userService = $userService;
        $this->passwordEncoderService = $passwordEncoderService;
    }

    /**
     * @Rest\Get("/")
     * @return View
     */
    public function index(): View
    {
        $users = $this->userService->getUsers();
        return View::create($users, Response::HTTP_OK);
    }


    /**
     * @Rest\Post("/new")
     * @param Request $request
     * @throws Exception
     * @return View
     */
    public function new(Request $request): View
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user, [
            'csrf_protection' => false,
            'method' => 'POST'
        ]);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();
            $user->setRole("ROLE_USER");
            $user = $this->passwordEncoderService->encodePassword($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return View::create($user, Response::HTTP_CREATED);
        }

        return View::create($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Get("/{id}")
     * @param $id
     * @return View view
     */
    public function show($id): View
    {
        $user = $this->userService->getUserById($id);
        return View::create($user, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/{id}/edit")
     * @param User $user
     * @param Request $request
     * @return View
     */
    public function edit(User $user, Request $request): View
    {
        $form = $this->createForm(UserType::class, $user, [
            'csrf_protection' => false,
            'method' => 'PUT'
        ]);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user = $this->passwordEncoderService->encodePassword($user);
            $this->getDoctrine()->getManager()->flush();
            return View::create($user, Response::HTTP_OK);
        }

        return View::create($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Delete("/{id}")
     * @param $id
     * @return View view
     */
    public function delete($id): View
    {
        $user = $this->userService->getUserById($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}