<?php
/**
 * Created by PhpStorm.
 * User: martino
 * Date: 30.03.20
 * Time: 21:35
 */

namespace App\Controller\Api;

use App\Entity\Courier;
use App\Entity\UserOrder;
use App\Form\UserOrderAdminStatusType;
use App\Form\UserOrderRecipientCourierStatusType;
use App\Form\UserOrderSenderCourierStatusType;
use App\Model\Status;
use App\Service\CourierOrderService;
use App\Service\CurrentUserService;
use App\Service\UserOrderService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/api/user-order")
 */
class UserOrderStatusApiController extends AbstractFOSRestController
{
    private $senderCourierStatus = [
        Status::CREATED,
        Status::RECEIVED_FROM_SENDER
    ];

    private $recipientCourierStatus = [
        Status::LOCALIZE_HEADQUARTERS,
        Status::DURING_TO_RECIPIENT
    ];

    private $currentUserService;
    private $userOrderService;
    private $courierOrderService;

    public function __construct(CurrentUserService $currentUserService, UserOrderService $userOrderService, CourierOrderService $courierOrderService)
    {
        $this->currentUserService = $currentUserService;
        $this->userOrderService = $userOrderService;
        $this->courierOrderService = $courierOrderService;
    }

    /**
     * @Rest\Get("/{id}/status")
     * @param $id
     * @throws \Exception
     * @return View
     */
    public function getById($id): View
    {
        return View::create($this->courierOrderService->getStatusesById($id), Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/{id}/status")
     * @param $userOrder
     * @param $request
     * @return View
     * @throws AccessDeniedException
     * @throws \Exception
     */
    public function setStatus(UserOrder $userOrder, Request $request)  :View
    {
        $currentUser = $this->currentUserService->getCurrentUser();
        $role = $currentUser->getRole();

        if($role == "ROLE_USER"){
            throw new AccessDeniedException('Unable to access this page!');
        }

        $currentCourier = null;
        $dataFormRequestStatus = null;
        if($role == "ROLE_COURIER") {
            $currentCourier = $this->getDoctrine()->getRepository(Courier::class)
                ->findOneBy(['user'=> $currentUser]);
            if($currentCourier == null){
                throw new AccessDeniedException('Unable to access this page!');
            }

            $courierOrder = $this->getDoctrine()->getRepository(UserOrder::class)
                ->findBy(['id'=> $userOrder->getId(),
                    'courier'=>$currentCourier]);
            if($courierOrder == null) {
                throw new AccessDeniedException('Unable to access this page!');
            }
        }

        if($role == "ROLE_ADMIN" || $role == "ROLE_COURIER"){
            $dataFormRequestStatus = $request->request->get("status");
            $form = $this->createForm(UserOrderAdminStatusType::class, $userOrder, [
                'csrf_protection' => false,
                'method' => 'PUT'
            ]);
        }
        else{
            throw new AccessDeniedException('Unable to access this page!');
        }

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $districtFromSenderDetails = $userOrder->getSenderDetails()->getDistrict();
            if($userOrder->getCourier()->getDistrict() != $districtFromSenderDetails && in_array($dataFormRequestStatus, $this->senderCourierStatus)){               //w bloku kodu if stworzylismy logike,która odpowiada za zmiane kuriera 1 na kureira 2
                $courierForDistrict = $this->getRandomCourierForDistrict($districtFromSenderDetails);
                $userOrder->setCourier($courierForDistrict);
            }

            $districtFromRecipientDetails = $userOrder->getRecipientDetails()->getDistrict();
            if($userOrder->getCourier()->getDistrict() != $districtFromRecipientDetails && in_array($dataFormRequestStatus, $this->recipientCourierStatus)){               //w bloku kodu if stworzylismy logike,która odpowiada za zmiane kuriera 1 na kureira 2
                $courierForDistrict = $this->getRandomCourierForDistrict($districtFromRecipientDetails);
                $userOrder->setCourier($courierForDistrict);
            }

            if(Status::LOCALIZE_HEADQUARTERS == $dataFormRequestStatus){
                $courierForDistrict = $this->getRandomCourierForDistrict($districtFromRecipientDetails);
                $userOrder->setCourier($courierForDistrict);
            }

            if(Status::PROVIDED == $dataFormRequestStatus){
                $userOrder->setCourier(null);
            }

            $this->getDoctrine()->getManager()->flush();

            return View::create($userOrder, Response::HTTP_OK);
        }

        return View::create($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param $district
     * @return mixed
     * @throws \Exception
     */
    private function getRandomCourierForDistrict($district) {
        $couriers = $this->getDoctrine()->getRepository(Courier::class)
            ->findBy(['district' => $district]);

        if (!empty($couriers)) {
            return $couriers[rand(0, count($couriers) - 1)];
        } else {
            throw new \Exception("You should create courier for district: " . $district);
        }
    }
}