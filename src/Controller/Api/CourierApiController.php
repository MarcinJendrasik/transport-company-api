<?php

namespace App\Controller\Api;

use App\Entity\Courier;
use App\Entity\User;
use App\Form\CourierType;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api/courier")
 */
class CourierApiController extends AbstractController
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder) {
        $this->passwordEncoder = $passwordEncoder;
    }
    /**
     * @Rest\Get("/")
     * @return View
     */
    public function index():View
    {
        $couriers = $this->getDoctrine()
            ->getRepository(Courier::class)
            ->findAll();

        return View::create($couriers, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/new")
     * @param $request
     * @return View
     */
    public function new(Request $request):View
    {
        $courier = new Courier();
        $form = $this->createForm(CourierType::class, $courier, [
            'csrf_protection' => false,
            'method' => 'POST'
        ]);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {      // jest przesłany i jest ważny
            $entityManager = $this->getDoctrine()->getManager();

            $user = $courier->getUser();
            $user->setRole("ROLE_COURIER");
            $newEncodedPassword = $this->passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($newEncodedPassword);

            $entityManager->persist($courier);
            $entityManager->flush();

            return View::create($courier, Response::HTTP_CREATED);
        }

        return View::create($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Get("/{id}")
     * @param $courier
     * @return View
     */
    public function show(Courier $courier):View
    {
        return View::create($courier, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/{id}/edit")
     * @param $request
     * @param $courier
     * @return View
     */
    public function edit(Request $request, Courier $courier):View
    {
        $form = $this->createForm(CourierType::class, $courier, [
            'csrf_protection' => false,
            'method' => 'PUT'
        ]);

        $form->submit($request->request->all());

        $user = $courier->getUser();
        $newEncodedPassword = $this->passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($newEncodedPassword);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return View::create($courier, Response::HTTP_OK);
        }

        return View::create($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Delete("/{id}")
     * @param $courier
     * @return View
     */
    public function delete(Courier $courier):View
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($courier);
        $entityManager->flush();

        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}