<?php
/**
 * Created by PhpStorm.
 * User: martino
 * Date: 30.03.20
 * Time: 21:35
 */

namespace App\Controller\Api;

use App\Service\CurrentUserService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api/current-user")
 */
class CurrentUserController extends AbstractFOSRestController
{
    private $currentUserService;

    public function __construct(CurrentUserService $currentUserService)
    {
        $this->currentUserService = $currentUserService;
    }

    /**
     * @Rest\Get("/")
     */
    public function index(): View
    {
        return View::create($this->currentUserService->getCurrentUser(), Response::HTTP_OK);
    }
}