<?php

namespace App\Controller\Api;

use App\Entity\Courier;
use App\Entity\UserOrder;
use App\Service\CourierOrderService;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api/courier-order")
 */
class CourierOrderApiController extends AbstractController
{
    private $tokenStorage;
    private $courierOrderService;

    public function __construct(TokenStorageInterface $tokenStorage,
                                CourierOrderService $courierOrderService) {
        $this->tokenStorage = $tokenStorage;
        $this->courierOrderService = $courierOrderService;
    }

    /**
     * @Rest\Get("/")
     * @return View
     * @throws \Exception
     */
    public function index(): View
    {
        $courierOrderList = $this->courierOrderService->getCourierOrderAll();
        return View::create($courierOrderList, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/{id}")
     * @param $id
     * @return View
     * @throws \Exception
     */
    public function showBy($id) :View
    {
        $courierOrder = $this->courierOrderService->getCourierOrderById($id);
        return View::create($courierOrder, Response::HTTP_OK);
    }
}