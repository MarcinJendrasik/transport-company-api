<?php


namespace App\Controller\Api;

use App\Entity\Courier;
use App\Entity\User;
use App\Entity\UserOrder;
use App\Form\UserOrderByUserType;
use App\Form\UserOrderAdminStatusType;
use App\Form\UserOrderRecipientCourierStatusType;
use App\Form\UserOrderSenderCourierStatusType;
use App\Form\UserOrderType;
use App\Service\UserOrderService;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Status;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api/user-order")
 */
class UserOrderApiController extends AbstractController
{
    private $userOrderService;
    private $tokenStorage;

    public function __construct(UserOrderService $userOrderService, TokenStorageInterface $tokenStorage) {
        $this->userOrderService = $userOrderService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Rest\Get("/")
     * @return View
     */
    public function index(): View
    {
        $userOrderList = $this->userOrderService->getUserOrderList();
        return View::create($userOrderList, Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/new")
     * @throws \Exception
     * @param Request $request
     * @return View
     */
    public function new(Request $request): View
    {
        $userOrder = new UserOrder();
        $form = $this->createForm(UserOrderByUserType::class, $userOrder, [
            'csrf_protection' => false,
            'method' => 'POST'
        ]);

        $form->submit($request->request->all());

        if($form->isSubmitted() && $form->isValid()){
            $this->prepareData($userOrder);

            $districtFromSenderDetails = $userOrder->getSenderDetails()->getDistrict();

            $courierForDistrict = $this->getRandomCourierForDistrict($districtFromSenderDetails);

            $userOrder->setCourier($courierForDistrict);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userOrder);
            $entityManager->flush();

            return View::create($userOrder, Response::HTTP_CREATED);
        }

        return View::create($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param UserOrder $userOrder
     * @throws \Exception
     */
    public function prepareData(UserOrder $userOrder): void
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();
        $userOrder->setStatus(Status::CREATED);
        $userOrder->setCreatedAt(new \DateTime());

        $userOrder->setUser($currentUser);
    }

    /**
     * @Rest\Get("/{id}")
     * @param $id
     * @return View
     */
    public function show($id) :View
    {
        $userOrder = $this->userOrderService->getUserOrderById($id);
        return View::create($userOrder, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/{id}/edit/user")
     * @param $request
     * @param $id
     * @return View
     */
    public function edit(Request $request, $id): View
    {
        $userOrder = $this->userOrderService->getUserOrderById($id);

        $form = $this->createForm(UserOrderByUserType::class, $userOrder, [
            'csrf_protection' => false,
            'method' => 'PUT'
        ]);

        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $this-> getDoctrine()->getManager()->flush();

            return View::create($userOrder, Response::HTTP_OK);
        }

        return View::create($form->getErrors(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Delete("/{id}")
     * @param $id
     * @return View
     */
      public function delete($id): View
      {
          $userOrder = $this->userOrderService->getUserOrderById($id);
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->remove($userOrder);
          $entityManager->flush();

         return View::create(null, Response::HTTP_NO_CONTENT);
      }

    /**
     * @param $district
     * @return mixed
     * @throws \Exception
     */
    private function getRandomCourierForDistrict($district) {
        $couriers = $this->getDoctrine()->getRepository(Courier::class)
            ->findBy(['district' => $district]);

        if (!empty($couriers)) {
            return $couriers[rand(0, count($couriers) - 1)];
        } else {
            throw new \Exception("You should create courier for district: " . $district);
        }
    }
}

