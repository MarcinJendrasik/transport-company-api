<?php

namespace App\Controller;

use App\Entity\Courier;
use App\Entity\UserOrder;
use App\Service\CourierOrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * @Route("/courier-order")
 */
class CourierOrderController extends AbstractController
{
    private $tokenStorage;
    private $courierOrderService;

    public function __construct(TokenStorageInterface $tokenStorage,
                                CourierOrderService $courierOrderService) {
        $this->tokenStorage = $tokenStorage;
        $this->courierOrderService = $courierOrderService;
    }

    /**
     * @Route("/", name="courier_order_index", methods={"GET"})
     * @return Response
     * @throws \Exception
     */
    public function index(): Response
    {
        $courierOrderList = $this->courierOrderService->getCourierOrderAll();
        return $this->render('courier_order/index.html.twig',  [
            'courierOrderList' => $courierOrderList
        ]);
    }

    /**
     * @Route("/{courierId}", name="courier_order_index_by_courier", methods={"GET"})
     * @param $courierId
     * @return Response
     * @throws \Exception
     */
    public function indexBy($courierId): Response
    {
        $courierOrderList = $this->courierOrderService->getCourierOrderList($courierId);
        return $this->render('courier_order/index_by_courier.html.twig',  [
            'courierOrderList' => $courierOrderList,
            'courierId' => $courierId,
        ]);
    }

    /**
     * @Route("/{courierId}/user-order/{userOrderId}", name="courier_order_show", methods={"GET"})
     * @throws \Exception
     */
    public function showBy($courierId, $userOrderId) :Response
    {
          $courierOrder = $this->courierOrderService->getCourierOrderById($userOrderId);
        return $this->render('courier_order/show.html.twig', [
            'courierOrder' => $courierOrder,
            'courierId' => $courierId
        ]);
    }
}